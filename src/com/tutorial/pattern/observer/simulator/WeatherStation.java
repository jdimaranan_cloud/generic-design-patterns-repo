package com.tutorial.pattern.observer.simulator;

import com.tutorial.pattern.observer.model.CurrentWeatherConditionsDisplay;
import com.tutorial.pattern.observer.model.WeatherData;
import com.tutorial.pattern.observer.model.WeatherStatisticsDisplay;

public class WeatherStation {

	public static void main(String[] args) {
		WeatherData weatherData = new WeatherData();
		
		CurrentWeatherConditionsDisplay currentWeatherConditionsDisplay = new CurrentWeatherConditionsDisplay(weatherData);
		WeatherStatisticsDisplay weatherStatisticsDisplay = new WeatherStatisticsDisplay(weatherData);
		
		weatherData.setMeasurements(80, 65, 30.4f);
		weatherData.setMeasurements(82, 70, 29.2f);
	}

}
