package com.tutorial.pattern.observer.model;

import java.util.ArrayList;

public class WeatherData implements Subject {
	
	public ArrayList<Observer> observerList;
	private float temperature;
	private float humidity;
	private float pressure;
	
	public WeatherData() {
		observerList = new ArrayList<Observer>();
	}

	@Override
	public void registerObserver(Observer anyObserver) {
		if (null != observerList) {
			observerList.add(anyObserver);
		}
	}

	@Override
	public void removeObserver(Observer anyObserver) {
		int indexOfObserver = observerList.indexOf(anyObserver);
		
		if (indexOfObserver >= 0) {
			observerList.remove(indexOfObserver);
		}

	}

	/**
	 * Used to update all observers when there is a Subject state change
	 */
	@Override
	public void notifyObservers() {
		for (Observer anyObserver : observerList) {
			anyObserver.update(temperature, humidity, pressure);
		}
	}
	
	
	/**
	 * Notify the observers whenever there is Subject state change
	 */
	public void measurementsChange() {
		notifyObservers();
	}
	
	
	/**
	 * Use this method to simulate reading data from a Weather Station
	 * So this is the data that we need to monitor if it is being notified to Observers
	 */
	public void setMeasurements(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		//Process indicating state change
		measurementsChange();
		
	}

}
