package com.tutorial.pattern.observer.model;

public interface DisplayElement {
	public void display();
}
