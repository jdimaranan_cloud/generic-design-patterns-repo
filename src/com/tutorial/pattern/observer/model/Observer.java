package com.tutorial.pattern.observer.model;

public interface Observer {
	public void update(float temperature, float humidity, float pressure);

}
