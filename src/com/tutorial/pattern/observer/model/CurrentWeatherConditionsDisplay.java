package com.tutorial.pattern.observer.model;

public class CurrentWeatherConditionsDisplay implements DisplayElement, Observer {
	private Subject weatherData;
	private float temperature;
	private float humidity;
	
	
	
	/**
	 * The Observer constructor is passed the WeatherData object (subject) 
	 * and we use it to register the Observer
	 * @param weatherData
	 */
	public CurrentWeatherConditionsDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}
	
	@Override
	public void update(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		display();

	}

	@Override
	public void display() {
		System.out.println("CurrentWeatherConditionsDisplay. Temperature at " + this.temperature + " F degrees and humidity at % " + this.humidity);

	}

}
