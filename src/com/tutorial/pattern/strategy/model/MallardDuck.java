package com.tutorial.pattern.strategy.model;

import com.tutorial.pattern.strategy.behavior.fly.FlyWithWings;
import com.tutorial.pattern.strategy.behavior.quack.Quack;

public class MallardDuck extends Duck {
	
	public MallardDuck() {
		super.flyBehavior = new FlyWithWings();
		super.quackBehavior = new Quack();
	}

	@Override
	public void display() {
		System.out.println("I'm doing display : MallardDuck");
	}

}
