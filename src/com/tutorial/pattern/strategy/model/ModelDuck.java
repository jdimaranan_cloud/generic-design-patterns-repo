package com.tutorial.pattern.strategy.model;

import com.tutorial.pattern.strategy.behavior.fly.FlyNoWay;
import com.tutorial.pattern.strategy.behavior.quack.MuteQuack;

public class ModelDuck extends Duck {

	
	public ModelDuck() {
		super.flyBehavior = new FlyNoWay();
		super.quackBehavior = new MuteQuack();
	}
	
	@Override
	public void display() {
		System.out.println("I'm doing display : ModelDuck");
	}

}
