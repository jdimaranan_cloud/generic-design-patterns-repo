package com.tutorial.pattern.strategy.behavior.quack;

public class MuteQuack implements QuackBehavior {

	@Override
	public void quack() {
		System.out.println("I'm doing QuackBehavior : MuteQuack");
	}

}
