package com.tutorial.pattern.strategy.behavior.quack;

/**
 * Encapsulated quack behavior
 *
 */
public interface QuackBehavior {
	public void quack();
}
