package com.tutorial.pattern.strategy.behavior.quack;

public class Quack implements QuackBehavior {

	public void quack() {
		System.out.println("I'm doing QuackBehavior : Quack");

	}

}
