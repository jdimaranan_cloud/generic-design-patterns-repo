package com.tutorial.pattern.strategy.behavior.quack;

public class Squeak implements QuackBehavior {

	public void quack() {
		System.out.println("I'm doing QuackBehavior : Squeak");

	}

}
