package com.tutorial.pattern.strategy.behavior.fly;

public class FlyRocketPowered implements FlyBehavior {

	@Override
	public void fly() {
		System.out.println("I'm doing FlyBehavior : FlyRocketPowered");
	}

}
