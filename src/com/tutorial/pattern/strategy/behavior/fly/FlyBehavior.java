package com.tutorial.pattern.strategy.behavior.fly;

/**
 * Encapsulated fly behavior
 */
public interface FlyBehavior {
	public void fly();
}
