package com.tutorial.overriding;

public class EmployeeOverridingTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee employee1 = new Employee();
		Employee employee2 = new SalaryEmployee();
		
		employee1.setAge(25);
		employee2.setAge(35);
		
		System.out.println("employee1 - Employee age: " + employee1.getAge());
		System.out.println("employee2 - SalaryEmployee age: " + employee2.getAge());
		
		//Demonstrating that private variables are not visible
		//employee1.age;
		
		//Another set of testing
		Employee employee3 = new HourlyEmployee();
		
		System.out.println("employee1 - Employee computePay " + employee1.computePay());
		System.out.println("employee3 - HourlyEmployee computePay: " + employee3.computePay());
	}

}
