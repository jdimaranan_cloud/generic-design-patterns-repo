package com.tutorial.overriding;

public class HippoToString {
	private String name;
	private Double weight;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	
	public HippoToString() {	
	}
	
	public HippoToString(String name, Double weight) {
		this.name = name;
		this.weight = weight;
	}
	
	@Override
	public String toString() {
		return "HippoToString [name=" + name + ", weight=" + weight + "]";
	}
	
	
	
	
}
