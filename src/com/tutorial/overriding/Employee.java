package com.tutorial.overriding;

public class Employee {
	private String name;
	private int age;
	private int zip;
	private float pay = 500.0f;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}
	
	public float computePay() {
		return pay;
	}

}
