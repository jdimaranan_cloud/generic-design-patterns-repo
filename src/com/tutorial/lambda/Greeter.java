package com.tutorial.lambda;

public class Greeter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Greeter greeter = new Greeter();
		HelloWorldGreeting helloWorldGreeting = new HelloWorldGreeting();
		greeter.greet(helloWorldGreeting);

	}
	
	public void greet(Greeting greeting) {
		greeting.perform();
	}
	
	/*
	 * aBlockOfCode = () -> {System.out.println("This is a lambda");}
	 * 
	 * */
	
	//greetingFunction = () -> System.out.println("Hello lambda");
	//Encapsulated a function into a variable that can be passed around
	
	//greet(greetingFunction);
	
	//greet(() -> {System.out.println("Hello lambda");})
	
	/*
	 * doubleNumberFunction = public int doubleFunction(int a) {
	 * 	return a * 2;
	 * }
	 * 
	 * doubleNumberFunction =  (int a) -> {
	 * 	return a * 2;
	 * }
	 * 
	 * As this is just one line we could skip the return
	 * doubleNumberFunction = (int a) -> a * 2;
	 * 
	 * addFunction = (int a, int b) -> a + b;
	 * 
	 * safeDivideFunction
	 */
	

}
