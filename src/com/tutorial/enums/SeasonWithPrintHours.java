package com.tutorial.enums;

public enum SeasonWithPrintHours {
	
	SUMMER {
		public void printHours() {System.out.println("Summer hours : 9am to 7pm");}
	},
	FALL {
		public void printHours() {System.out.println("Fall hours : 9am to 5pm");}
	},
	SPRING, WINTER;
	
	public void printHours() {
		System.out.println("Default hours for any season");
	}
	
	//With this option, every enum value is required to implement the abstract method below
	//public abstract void printHours();

}
