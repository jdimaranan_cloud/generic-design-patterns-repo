package com.tutorial.enums;

public enum SeasonExpectedVisitors {
	WINTER("Low"), SPRING("Medium"), FALL("Medium"), SUMMER("High");

	private String expectedVisitors;
	
	/* Constructor is private as it can only be called within the enum */
	private SeasonExpectedVisitors(String expectedVisitors) {
		this.expectedVisitors = expectedVisitors;
	}

	public void printExpectedVisitors() {
		System.out.println(this.expectedVisitors);
	}
}
