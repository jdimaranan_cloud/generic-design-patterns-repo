package com.tutorial.enums;

public enum EnumConstructorOnlyOnce {
	ONCE(true);
	
	private EnumConstructorOnlyOnce(boolean AnyBooleanValue) {
		System.out.println("Proof of calling constructor");
	}

}
