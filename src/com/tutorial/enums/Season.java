package com.tutorial.enums;

public enum Season {
	WINTER, SPRING, SUMMER, FALL
	
	/**An enum cannot be extended **/
}
