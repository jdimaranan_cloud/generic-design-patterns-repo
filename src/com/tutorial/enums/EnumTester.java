package com.tutorial.enums;

public class EnumTester {

	public static void main(String[] args) {

		Season season = Season.SUMMER;

		System.out.println("Current season : " + season);

		switch (season) {
		case WINTER:
			System.out.println("Time to ski");
			break;
		case SUMMER:
			System.out.println("Time for the pool");
			break;
		default:
			System.out.println("What season is it?");
		}
		
		SeasonExpectedVisitors.SUMMER.printExpectedVisitors();
		
		//
		EnumConstructorOnlyOnce first = EnumConstructorOnlyOnce.ONCE;
		EnumConstructorOnlyOnce second = EnumConstructorOnlyOnce.ONCE;
		
		//
		SeasonWithPrintHours.SUMMER.printHours();;
		
	}

}
