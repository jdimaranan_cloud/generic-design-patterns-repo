package com.tutorial.nested;

public class OuterWithSameVariable {
	
	private String sameVariable = "Outer class variable";
	
	public class InnerWithSameVariable {
		private String sameVariable = "Inner class variable";
		
		public void printVariables() {
			System.out.println(InnerWithSameVariable.this.sameVariable);
			System.out.println(OuterWithSameVariable.this.sameVariable);
		}
	}

}
