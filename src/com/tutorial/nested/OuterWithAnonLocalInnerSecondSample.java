package com.tutorial.nested;

public class OuterWithAnonLocalInnerSecondSample {
	
	public interface SaleTodayOnlyInterface {
		//Interfaces require public methods.  By default they are public
		public int dollarsOff();
	}
	
	public int admission(int basePrice) {
		SaleTodayOnlyInterface sale = new SaleTodayOnlyInterface() {
			public int dollarsOff() {
				return 3;
			}
		};
		
		return basePrice - sale.dollarsOff();
	}

}
