package com.tutorial.nested;

public class OuterWithAnnonymousLocalInner {

	public abstract class SaleTodayOnly {
		abstract int dollarsOff();
	}
	
	public int admission(int basePrice) {
		//Anonymous inner class are required to extend an existing class or implement an existing interface
		SaleTodayOnly sale = new SaleTodayOnly() {
			public int dollarsOff() 
			{
				return 3;
			}
		}; //Pay special attention to this semicolon, this is required for local variable declarations
		return basePrice - sale.dollarsOff();
	}

}
