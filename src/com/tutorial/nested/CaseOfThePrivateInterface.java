package com.tutorial.nested;

public class CaseOfThePrivateInterface {
	
	//This could also be public
	private interface Secret{
		public void shhh();
	}
	
	public class InnerClassImplementingInnerInterface implements Secret {
		@Override
		public void shhh() {
			System.out.println("Implementing Inner Interface");
		}
	}

}
