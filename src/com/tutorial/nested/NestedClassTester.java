package com.tutorial.nested;

import com.tutorial.nested.CaseOfThePrivateInterface.InnerClassImplementingInnerInterface;
import com.tutorial.nested.OuterWithNestedMemberInner.Inner;
import static com.tutorial.nested.OuterWithNestedStatic.Nested;
import com.tutorial.nested.OuterWithSameVariable.InnerWithSameVariable;

public class NestedClassTester {
	
	public static void main(String[] args) {
		OuterWithNestedMemberInner outerWithNestedMemberInner = new OuterWithNestedMemberInner();
		
		//This is first option to call an nested member inner class
		//nestedInnerMember.callInnerClass();
		
		//This is second option to call a nested member inner class
		Inner inner = outerWithNestedMemberInner.new Inner();
		inner.go();
		
		
		//Test showing how same variable names behave in nested member inner class
		OuterWithSameVariable outerWithSameVariable = new OuterWithSameVariable();
		InnerWithSameVariable innerWithSameVariable = outerWithSameVariable.new InnerWithSameVariable();
		
		innerWithSameVariable.printVariables();
		
		//Test for nested private interface
		CaseOfThePrivateInterface caseOfThePrivateInterface = new CaseOfThePrivateInterface();
		InnerClassImplementingInnerInterface innerClassImplementingInnerInterface = caseOfThePrivateInterface.new InnerClassImplementingInnerInterface(); 
		
		innerClassImplementingInnerInterface.shhh();
		
		//Test for nested local inner class
		OuterWithNestedLocalInner outerWithNestedLocalInner = new OuterWithNestedLocalInner();
		outerWithNestedLocalInner.calculateLength();
		
	}
}
