package com.tutorial.nested;

public class OuterWithNestedMemberInner {
	private String greeting = "Hi";
	
	//A NESTED member inner class can have all the different access modifiers
	protected class Inner {
		public int repeat = 4;
		
		public void go() {
			for (; repeat !=0 ; repeat = --repeat) {
				System.out.println(greeting);
			}
		}
		
	}
	
	public void callInnerClass() {
		Inner inner = new Inner();
		inner.go();
	}

}
