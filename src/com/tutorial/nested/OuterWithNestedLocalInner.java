package com.tutorial.nested;

public class OuterWithNestedLocalInner {
	private int length = 5;
	
	public void calculateLength() {
		final int width = 20;
		// If the class is declared within a local method, then it is a local inner
		
		//It does not have an access modifier
		//It could not be declared as static
		class LocalInner {
			public void multiply() {
				System.out.println("We are accessing LocalInner.multiply : "+  width * length);
			}
		}
		
		LocalInner localInner = new LocalInner();
		localInner.multiply();
	}
}
